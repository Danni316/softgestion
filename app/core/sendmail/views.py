import smtplib
import uuid
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.template.loader import render_to_string

from config import settings


def send_mail_text():
    try:
        # Establecemos conexion con el servidor smtp de gmail
        mailServer = smtplib.SMTP(settings.EMAIL_HOST, 587)
        print(mailServer.ehlo())
        mailServer.starttls()
        print(mailServer.ehlo())
        mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        print('Conectado!')

        email_to = 'danni.316@hotmail.com.ar'

        # Construimos el mensaje simple
        mensaje = MIMEText("""Este es el mensaje""")
        mensaje['From'] = settings.EMAIL_HOST_USER
        mensaje['To'] = email_to
        mensaje['Subject'] = "Tienes un correo desde django"

        # Envio del mensaje
        mailServer.sendmail(settings.EMAIL_HOST_USER,
                            settings.EMAIL_HOST_USER,
                            mensaje.as_string())

        print('Correo enviado correctamente =)')

    except Exception as e:
        print(e)


def send_mail_html():
    try:
        # Establecemos conexion con el servidor smtp de gmail
        mailServer = smtplib.SMTP(settings.EMAIL_HOST, 587)
        print(mailServer.ehlo())
        mailServer.starttls()
        print(mailServer.ehlo())
        mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        print('Conectado!')

        email_to = 'danni.316@hotmail.com.ar'

        # Construimos el mensaje simple
        mensaje = MIMEMultipart("""Este es el mensaje""")
        mensaje['From'] = settings.EMAIL_HOST_USER
        mensaje['To'] = email_to
        mensaje['Subject'] = "Tienes un correo desde django"

        content = render_to_string('reset_password/reset_password.html')
        mensaje.attach(MIMEText(content, 'html'))

        # Envio del mensaje
        mailServer.sendmail(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_USER, mensaje.as_string())

        print('Correo enviado correctamente =)')

    except Exception as e:
        print(e)

