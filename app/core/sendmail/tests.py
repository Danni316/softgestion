from config.wsgi import *
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

from django.template.loader import render_to_string

from config import settings

# from django.conf import settings as s
# s.configure()


def send_mail():
    try:
        # Establecemos conexion con el servidor smtp de gmail
        mailServer = smtplib.SMTP(settings.EMAIL_HOST, 587)
        print(mailServer.ehlo())
        mailServer.starttls()
        print(mailServer.ehlo())
        mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        print(mailServer.ehlo())
        print('Conectado!')

        email_to = 'danni.316@hotmail.com.ar'

        # Construimos el mensaje simple
        mensaje = MIMEText("""Este es el mensaje""")
        mensaje['From'] = settings.EMAIL_HOST_USER
        mensaje['To'] = email_to
        mensaje['Subject'] = "Tienes un correo desde django"

        # Envio del mensaje
        mailServer.sendmail(settings.EMAIL_HOST_USER, email_to, mensaje.as_string())

        print('Correo enviado correctamente a ' + email_to)

    except Exception as e:
        print(e)


def send_mail_html():
    try:
        # Establecemos conexion con el servidor smtp de gmail
        mailServer = smtplib.SMTP(settings.EMAIL_HOST, 587)
        print(mailServer.ehlo())
        mailServer.starttls()
        print(mailServer.ehlo())
        mailServer.login(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_PASSWORD)
        print('Conectado!')

        email_to = 'danni.316@hotmail.com.ar'

        # Construimos el mensaje simple
        mensaje = MIMEMultipart("""Este es el mensaje""")
        mensaje['From'] = settings.EMAIL_HOST_USER
        mensaje['To'] = email_to
        mensaje['Subject'] = "Tienes un correo desde django"

        content = render_to_string('reset_password/reset_password.html')
        mensaje.attach(MIMEText(content, 'html'))

        # Envio del mensaje
        mailServer.sendmail(settings.EMAIL_HOST_USER, settings.EMAIL_HOST_USER, mensaje.as_string())

        print('Correo enviado correctamente =)')

    except Exception as e:
        print(e)


send_mail_html()