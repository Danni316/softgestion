from django import forms

from core.user.models import User


class ResetPasswordForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={
        'placeholder': 'Ingrese un nombre de usuario',
        'class': 'form-control',
        'autocomplete': 'off'
    }))

    def clean(self):
        cleanned = super().clean()
        if not User.objects.filter(username=cleanned['username']).exists():
            # codigo para formatear el mensaje de error a mostrar
            self._errors['error'] = self._errors.get('error', self.error_class())
            self._errors['error'].append(' El usuario no existe')
            # raise forms.ValidationError('El usuario no existe')
        return cleanned

    def get_user(self):
        username = self.cleaned_data.get('username')
        return User.objects.get(username=username)


class ChangePasswordForm(forms.Form):
    password = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'Ingrese una contraseña',
        'class': 'form-control',
        'autocomplete': 'off'
    }))

    confirmPassword = forms.CharField(widget=forms.PasswordInput(attrs={
        'placeholder': 'Repita la contraseña',
        'class': 'form-control',
        'autocomplete': 'off'
    }))

    def clean(self):
        cleanned = super().clean()
        password = cleanned['password']
        confirmPassword = cleanned['confirmPassword']
        if password != confirmPassword:
            # codigo para formatear el mensaje de error a mostrar
            self._errors['error'] = self._errors.get('error', self.error_class())
            self._errors['error'].append(' Las contraseñas deben ser iguales')
            # raise forms.ValidationError('El usuario no existe')
        return cleanned

