from django import forms
from django.forms import ModelForm

from core.user.models import User


class UserForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # se utiliza para eliminar la renderizacion dinamica de los componentes de la clase bootstrap
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['first_name'].widget.attrs['autofocus'] = True
        self.fields['groups'].widget.attrs['class'] = 'form-control select2'

    class Meta:
        model = User
        fields = 'first_name', 'last_name', 'email', 'username', 'password', 'image', 'groups',
        # personaliza los campos
        widgets = {
            'first_name': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese nombre',
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese apellido',
                }
            ),
            'email': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese su email',
                }
            ),
            'username': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese username',
                }
            ),
            'password': forms.PasswordInput(render_value=True,
                attrs={
                    'placeholder': 'Ingrese contraseña',
                }
            ),
            'groups': forms.SelectMultiple(
                attrs={
                    'style': 'width: 100',
                    'multiple': 'multiple',
            })
        }
        exclude = ['user_permissions', 'last_login', 'date_joined', 'is_superuser', 'is_active', 'is_staff']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                password = self.cleaned_data['password']
                usuario = form.save(commit=False)
                if usuario.pk is None:
                    usuario.set_password(password)
                else:
                    user = User.objects.get(pk=usuario.pk)
                    if user.password != password:
                        usuario.set_password(password)
                usuario.save()
                usuario.groups.clear()
                for group in self.cleaned_data['groups']:
                    usuario.groups.add(group)
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    # metodo que contiene los errores
    # comprueba que si tiene menos de 0 caracteres y agrega el error a la variable diccionario cleanned
    def clean(self):
        cleanned = super().clean()
        if len(cleanned['first_name']) <= 0:
            raise forms.ValidationError('Validación xxx')
            # self.add_error('name', 'Le faltan caracteres')
        print(cleanned)
        return cleanned


class UserProfileForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # se utiliza para eliminar la renderizacion dinamica de los componentes de la clase bootstrap
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'
        self.fields['first_name'].widget.attrs['autofocus'] = True

    class Meta:
        model = User
        fields = 'first_name', 'last_name', 'email', 'username', 'password', 'image'
        # personaliza los campos
        widgets = {
            'first_name': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese nombre',
                }
            ),
            'last_name': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese apellido',
                }
            ),
            'email': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese su email',
                }
            ),
            'username': forms.TextInput(
                attrs={
                    'placeholder': 'Ingrese username',
                }
            ),
            'password': forms.PasswordInput(render_value=True,
                attrs={
                    'placeholder': 'Ingrese contraseña',
                }
            ),
        }
        exclude = ['user_permissions', 'last_login', 'date_joined', 'is_superuser', 'is_active', 'is_staff', 'groups']

    def save(self, commit=True):
        data = {}
        form = super()
        try:
            if form.is_valid():
                password = self.cleaned_data['password']
                usuario = form.save(commit=False)
                if usuario.pk is None:
                    usuario.set_password(password)
                else:
                    user = User.objects.get(pk=usuario.pk)
                    if user.password != password:
                        usuario.set_password(password)
                usuario.save()
            else:
                data['error'] = form.errors
        except Exception as e:
            data['error'] = str(e)
        return data

    # metodo que contiene los errores
    # comprueba que si tiene menos de 0 caracteres y agrega el error a la variable diccionario cleanned
    def clean(self):
        cleanned = super().clean()
        if len(cleanned['first_name']) <= 0:
            raise forms.ValidationError('Validación xxx')
            # self.add_error('name', 'Le faltan caracteres')
        print(cleanned)
        return cleanned
